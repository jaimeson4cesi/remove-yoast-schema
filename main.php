<?php namespace RemoveYoastSchema
{
	/*
	Plugin Name:  Remove Yoast Schema
	Plugin URI:   https://www.jaimeson-waugh.com
	Description:  Removes schema automatically added by Yoast
	Version:      1.0
	Author:       Jaimeson Waugh
	Author URI:   https://www.jaimeson-waugh.com
	License:      GPL2
	License URI:  https://www.gnu.org/licenses/gpl-2.0.html
	Text Domain:  remove-yoast-schema
	Domain Path:  /languages

	"Remove Yoast Schema" is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	any later version.

	"Remove Yoast Schema" is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with "Remove Yoast Schema". If not, see https://www.gnu.org/licenses/gpl-2.0.html.
	*/

	function RemoveYoastSchema()
	{
		// Change this so it gives Yoast nothing, so it prints nothing.
		return [];
	}
	add_filter( 'wpseo_json_ld_output', '\RemoveYoastSchema\RemoveYoastSchema' );
}
